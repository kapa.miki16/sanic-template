# Make creation service fast again

Cookiecutter for creation services with Sanic Framework

#### Version: 0.1.0

## Create new project

Install cookiecutter locally

```$ pip install cookiecutter```

Create new project

```$ cookiecutter https://gitlab.com/kapa.miki16/sanic-template```
