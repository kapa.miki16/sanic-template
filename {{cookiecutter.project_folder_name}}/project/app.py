from tortoise.contrib.sanic import register_tortoise
from sanic import Sanic
from sanic.response import text

from project import database


app = Sanic(name='{{ cookiecutter.project_name }}')


register_tortoise(app, config=database.TORTOISE_CONFIG)


@app.get("/")
async def hello_world(request):
    return text("Hello, world.")



if __name__ == "__main__":
    app.run(host='0.0.0.0')
