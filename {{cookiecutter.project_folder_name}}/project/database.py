from . import settings
from .utils import add_url_params


TORTOISE_CONFIG = {
    'connections': {
        'default': add_url_params(
            settings.DATABASE_URL, {
                'minsize': settings.MIN_DATABASE_POOL_SIZE,
                'maxsize': settings.MAX_DATABASE_POOL_SIZE,
            }
        ),
    },
    'apps': {
        'models': {
            'models': ['aerich.models', 'project.models'],
            'default_connection': 'default',
        },
    },
    'use_tz': True,
}
