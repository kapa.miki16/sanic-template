from furl import furl


def add_url_params(url: str, params: dict) -> str:
    return furl(url).add(params).url
