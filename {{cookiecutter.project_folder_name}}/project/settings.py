from . import environ

DEBUG = environ.get('DEBUG', False)

SENTRY_DSN = environ.get('SENTRY_DSN', None)


DATABASE_URL = environ.get('DATABASE_URL')
MIN_DATABASE_POOL_SIZE = environ.get('MIN_DATABASE_POOL_SIZE', 10)
MAX_DATABASE_POOL_SIZE = environ.get('MAX_DATABASE_POOL_SIZE', 10)
