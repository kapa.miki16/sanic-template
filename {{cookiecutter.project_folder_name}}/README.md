# Setup
- Go to cloned project dir and run `docker-compose build` command, after installing run `docker-compose up`.
- Check all is running OK: `docker-compose ps`. All services must be in `Up` state.
- `docker-compose exec {{ cookiecutter.project_name }} migrate`

Then do the following commands: `$ docker-compose exec {{ cookiecutter.project_name }} migrate`. 
Will call a bin/migrate script that creates a default tenant and applies migrations. 


If you made a change to some script inside `bin/` directory you have to rebuild the whole 
`{{ cookiecutter.project_name }}` container, you can do it as so:
```
$ docker-compose kill {{ cookiecutter.project_name }}
$ docker-compose rm {{ cookiecutter.project_name }}
$ docker-compose build --no-cache {{ cookiecutter.project_name }}
```
It will rebuild the `{{ cookiecutter.project_name }}` container without the use of cache and make a new volume for `bin/` directory

If you changed fields in models you should do the following:
```
$ docker-compose exec {{ cookiecutter.project_name }} makemigrations
$ docker-compose exec {{ cookiecutter.project_name }} migrate
```

# Run Test

If you want to trigger tests you should run command
```
$ docker-compose exec {{ cookiecutter.project_name }} pytest -xv -s
```

# Description
{{ cookiecutter.readme_description }} 
